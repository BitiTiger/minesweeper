#!/bin/bash

# ensure python3 is installed
python3 --version 2> /dev/null
if [ $? != 0 ]; then
    # check if Arch
    grep "ID_LIKE=arch" /etc/os-release 2> /dev/null
    if [ $? == 0 ]; then
        echo "Installing Python 3 for Arch Linux..."
        sudo pacman -S python
    else
        # check if Debian
        grep "ID_LIKE=debian" /etc/os-release 2> /dev/null
        if [ $? == 0 ]; then
            echo "Installing Python 3 for Debian Linux..."
            sudo apt install python3
        else
            # display error
            echo "You must install Python 3 for your Linux distribution before playing this game."
            exit
        fi
    fi
fi

# run game
python3 "minesweeper.py"
